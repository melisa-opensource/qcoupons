# QCoupons (qcoupons)

QCoupons

## Install steps

1. [Install Quasar CLI](https://quasar.dev/quasar-cli/installation)
```bash
# Node.js >= 8.9.0 is required.
yarn global add @quasar/cli
# or
npm install -g @quasar/cli
```
2. Install the dependencies
```bash
yarn
# or
npm
```
3. Create file .env
```bash
cp env-example .env
```
4. Important update API_URL var
3. Run app
```bash
quasar dev
# or
npm run dev
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### Lint the files
```bash
yarn run lint
```

### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).
