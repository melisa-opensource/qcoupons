// https://medium.com/carbono/using-env-file-in-quasar-apps-72b56909302f
const DotEnv = require('dotenv')

module.exports = function (dev) {
  let enviroment = '.env'
  if (typeof process.env.NODE_ENV === 'string') {
    enviroment = process.env.NODE_ENV === 'stage' ? '.env.stage' : (process.env.NODE_ENV === 'development' ? '.env' : '.env.prod')
  } else {
    enviroment = dev ? '.env' : '.env.prod'
  }
  const parsedEnv = DotEnv.config({
    path: enviroment
  }).parsed
  // Let's stringify our variables
  for (let key in parsedEnv) {
    if (typeof parsedEnv[key] === 'string') {
      parsedEnv[key] = JSON.stringify(parsedEnv[key])
    }
  }
  return parsedEnv
}
