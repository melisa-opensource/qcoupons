export default {
  data () {
    return {
      columns: [
        {
          field: 'name',
          label: 'Nombre',
          align: 'left'
        },
        {
          field: 'score',
          label: 'Puntos'
        },
        {
          field: 'position',
          label: 'Posición'
        }
      ]
    }
  },
  mounted () {
    this.$store.dispatch('ranking/paging')
  },
  computed: {
    records () {
      return this.$store.getters['ranking/records']
    },
    loading () {
      return this.$store.getters['ranking/loading']
    },
    pagination () {
      return this.$store.getters['ranking/pagination']
    }
  }
}
