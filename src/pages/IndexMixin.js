export default {
  data () {
    return {
      columns: []
    }
  },
  mounted () {
    this.$store.dispatch('products/paging')
  },
  computed: {
    loading () {
      return this.$store.getters['products/loading']
    },
    products () {
      return this.$store.getters['products/records']
    },
    pagination () {
      return this.$store.getters['products/pagination']
    }
  }
}
