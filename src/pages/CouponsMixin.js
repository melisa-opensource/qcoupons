export default {
  data () {
    return {
      columns: [
        {
          field: 'code',
          label: 'Código',
          align: 'left'
        },
        {
          field: 'score',
          label: 'Puntuación',
          align: 'left'
        }
      ]
    }
  },
  mounted () {
    this.$store.dispatch('coupons/paging')
  },
  computed: {
    loading () {
      return this.$store.getters['coupons/paging']
    },
    records () {
      return this.$store.getters['coupons/records']
    }
  }
}
