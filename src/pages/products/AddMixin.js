export default {
  computed: {
    loading () {
      return this.$store.getters['products/loadingAdd']
    }
  },
  methods: {
    onClickBtnSave () {
      if (!this.$refs.form.isValidForm()) {
        return
      }
      this.$q.loading.show({
        message: 'Guardando producto'
      })
      const values = this.$refs.form.getFormValues()
      this.$store.dispatch('products/create', values)
        .then(this.onSuccessSave)
    },
    onSuccessSave (result) {
      this.$q.loading.hide()
      this.$router.push({
        name: 'products.report',
        params: result
      })
    }
  }
}
