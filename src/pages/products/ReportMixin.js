export default {
  mounted () {
    this.$store.dispatch('products/report', this.$route.params)
  },
  computed: {
    loading () {
      return this.$store.getters['products/loadingReport']
    },
    product () {
      return this.$store.getters['products/report']
    }
  },
  methods: {
    getComponentReport () {
      return import('src/components/products/Report')
    }
  }
}
