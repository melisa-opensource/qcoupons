export default {
  data () {
    return {
      showDrawerCart: false,
      versionApp: process.env.APP_VERSION
    }
  },
  created () {
    this.$root.$on('add-product-cart', this.onAddProductCart)
  },
  methods: {
    onAddProductCart () {
      this.showDrawerCart = true
    },
    onClickAppName () {
      this.$router.push({
        name: 'home'
      }).catch(() => {})
    }
  }
}
