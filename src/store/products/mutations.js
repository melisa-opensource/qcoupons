export function setRecords (state, payload) {
  state.records = payload
}

export function setLoading (state, payload) {
  state.loading = payload
}

export function setLoadingAdd (state, payload) {
  state.loadingAdd = payload
}

export function setLoadingReport (state, payload) {
  state.loadingReport = payload
}

export function setReport (state, payload) {
  state.report = payload
}
