export default {
  url: '/api/v1/products',
  records: [],
  loading: true,
  loadingAdd: false,
  loadingReport: true,
  report: null,
  pagination: {
    page: 1,
    rowsPerPage: 20
  }
}
