import faker from 'faker/locale/es_MX'

export function paging (context) {
  context.commit('setLoading', true)
  return new Promise((resolve, reject) => {
    const products = Array.from(Array(20), (_, x) => {
      const urlImage = 'https://placeimg.com/640/480/animals'
      const score = faker.random.number({
        min: 1,
        max: 7
      })
      return {
        id: faker.random.uuid(),
        name: faker.commerce.productName(),
        description: faker.lorem.paragraphs(2),
        url_image: `${urlImage}?nc=${x}`,
        score: score * 100
      }
    })
    setTimeout(() => {
      context.commit('setLoading', false)
      context.commit('setRecords', products)
      resolve(products)
    }, 1000)
  })
}

export function report (context) {
  context.commit('setLoadingReport', true)
  return new Promise((resolve, reject) => {
    const urlImage = 'https://placeimg.com/640/480/animals'
    const product = {
      id: faker.random.uuid(),
      name: faker.commerce.productName(),
      description: faker.lorem.paragraphs(2),
      url_image: urlImage,
      score: faker.random.number({
        min: 500,
        max: 2000
      })
    }
    setTimeout(() => {
      context.commit('setLoadingReport', false)
      context.commit('setReport', product)
      resolve(product)
    }, 1000)
  })
}
