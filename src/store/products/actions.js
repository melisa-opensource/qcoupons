// import faker from 'faker/locale/es_MX'
import * as http from 'src/helpers/StoreActions'
import axios from 'axios'
// import * as fake from './fake'

export function paging (context, payload) {
  // use fake data
  // return fake.paging(context)
  context.commit('setLoading', false)
  return new Promise((resolve, reject) => {
    return http.paging(context, payload)
      .then((result) => {
        if (!result.data.success) {
          return reject(result)
        }
        context.commit('setLoading', false)
        context.commit('setRecords', result.data.data)
        resolve(result.data.data)
      })
      .catch((result) => {
        reject(result)
      })
  })
}

export function report (context, payload) {
  // return fake.report(context)
  context.commit('setLoadingReport', true)
  return new Promise((resolve, reject) => {
    const url = `${context.getters.url}/${payload.id}`
    axios.get(url)
      .then((result) => {
        if (!result.data.success) {
          return reject(result)
        }
        context.commit('setLoadingReport', false)
        context.commit('setReport', result.data.data)
        resolve(result.data.data)
      })
      .catch((result) => {
        context.commit('setLoadingReport', false)
        reject(result)
      })
  })
}

export function create (context, payload) {
  context.commit('setLoadingAdd', true)
  return new Promise((resolve, reject) => {
    const url = context.getters.url
    axios.post(url, payload)
      .then((result) => {
        if (!result.data.success) {
          return reject(result)
        }
        context.commit('setLoadingAdd', false)
        resolve(result.data.data)
      })
      .catch((result) => {
        context.commit('setLoadingAdd', false)
        reject(result)
      })
  })
}
