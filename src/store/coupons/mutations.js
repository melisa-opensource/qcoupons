export function setLoadingAdd (state, payload) {
  state.loadingAdd = payload
}
export function setLoading (state, payload) {
  state.loading = payload
}
export function setRecords (state, payload) {
  state.records = payload
}
