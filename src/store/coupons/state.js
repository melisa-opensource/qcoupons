export default function () {
  return {
    url: '/api/v1/coupons',
    loadingAdd: false,
    loading: false,
    records: [],
    pagination: {
      page: 1,
      rowsPerPage: 20
    }
  }
}
