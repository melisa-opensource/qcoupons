export function loadingAdd (state) {
  return state.loadingAdd
}
export function loading (state) {
  return state.loading
}
export function url (state) {
  return state.url
}
export function pagination (state) {
  return state.pagination
}
export function records (state) {
  return state.records
}
