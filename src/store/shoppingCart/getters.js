export function records (state) {
  return state.records
}
export function url (state) {
  return state.url
}
export function loadingBuy (state) {
  return state.loadingBuy
}
export function show (state) {
  return state.show
}
export function totalRecords (state) {
  return state.records.length
}
