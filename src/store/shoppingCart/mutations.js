import { removeById } from 'src/helpers/Arrays'

export function add (state, payload) {
  state.records.push(payload)
}

export function remove (state, payload) {
  removeById(state.records, payload)
}
export function clearRecords (state) {
  state.records = []
}

export function setShow (state, payload) {
  state.show = payload
}
export function setLoadingBuy (state, payload) {
  state.loadingBuy = payload
}
