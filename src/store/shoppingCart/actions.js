import axios from 'axios'
import { findById, isUniqueId } from 'src/helpers/Arrays'

export function buy (context) {
  context.commit('setLoadingBuy', true)
  return new Promise((resolve, reject) => {
    const url = context.getters.url
    const idProducts = context.getters.records.map((product) => {
      return product.id
    })
    axios.post(url, {
      ids: JSON.stringify(idProducts)
    }).then((result) => {
      if (!result.data.success) {
        return reject()
      }
      context.commit('setLoadingBuy', false)
      context.commit('clearRecords')
      context.commit('setShow', false)
      resolve(result.data.data)
    }).catch((result) => {
      context.commit('setLoadingBuy', false)
      reject(result)
    })
  })
}

export function add (context, payload) {
  return new Promise((resolve, reject) => {
    const currentScore = context.rootGetters['users/score']
    if (currentScore < payload.score) {
      return reject()
    }
    if (!isUniqueId(context.getters.records, payload.id)) {
      return reject()
    }
    context.commit('setShow', true)
    context.commit('add', payload)
    const result = {
      data: {
        items: context.getters.records,
        score: currentScore - payload.score
      }
    }
    resolve(result)
  })
}

export function remove (context, payload) {
  return new Promise((resolve, reject) => {
    const index = findById(context.getters.records, payload)
    const product = context.getters.records[index]
    const score = context.rootGetters['users/score']
    context.commit('remove', payload)
    const result = {
      data: {
        score: score + product.score
      }
    }
    resolve(result)
  })
}

export function setShow (context, payload) {
  context.commit('setShow', payload)
}
