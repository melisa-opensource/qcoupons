export function records (state) {
  return state.records
}

export function loading (state) {
  return state.loading
}

export function loadingReport (state) {
  return state.loadingReport
}

export function pagination (state) {
  return state.pagination
}

export function report (state) {
  return state.report
}
