import faker from 'faker/locale/es_MX'

export function paging (context) {
  context.commit('setLoading', true)
  return new Promise((resolve, reject) => {
    const records = Array.from(Array(20), (_, x) => {
      return {
        id: faker.random.uuid(),
        name: faker.commerce.productName(),
        position: ++x,
        score: faker.random.number({
          min: 500,
          max: 2000
        })
      }
    })
    setTimeout(() => {
      context.commit('setLoading', false)
      context.commit('setRecords', records)
      resolve(records)
    }, 1000)
  })
}
