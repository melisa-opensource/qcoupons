export default {
  records: [],
  loading: true,
  loadingReport: true,
  report: null,
  pagination: {
    page: 1,
    rowsPerPage: 100
  }
}
