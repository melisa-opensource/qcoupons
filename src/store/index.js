import Vue from 'vue'
import Vuex from 'vuex'

// in real project loading dinamic stores
import products from './products'
import users from './users'
import usersCoupons from './users/coupons'
import ranking from './ranking'
import coupons from './coupons'
import shoppingCart from './shoppingCart'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

const store = new Vuex.Store({
  modules: {
    products,
    users,
    usersCoupons,
    ranking,
    coupons,
    shoppingCart
  }
})

export default store
