export function urlLogin (state) {
  return state.urlLogin
}
export function urlRegister (state) {
  return state.urlRegister
}
export function url (state) {
  return state.url
}
export function token (state) {
  return state.token
}
export function profile (state) {
  return state.profile
}
export function loadingProfile (state) {
  return state.loadingProfile
}
export function loadingCoupons (state) {
  return state.loadingCoupons
}
export function loadingProducts (state) {
  return state.loadingProducts
}
export function loadingLogin (state) {
  return state.loadingLogin
}
export function loadingLogout (state) {
  return state.loadingLogout
}
export function loadingRegister (state) {
  return state.loadingRegister
}
export function coupons (state) {
  return state.coupons
}
export function products (state) {
  return state.products
}
export function isAdmin (state) {
  return state.isAdmin
}
export const isAuthenticated = (state) => {
  return state.token
}
export function score (state) {
  return state.score
}
export function initialScore (state) {
  return state.initialScore
}
