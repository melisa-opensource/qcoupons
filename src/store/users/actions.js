import faker from 'faker/locale/es_MX'
import axios from 'axios'
// import * as fake from './fake'

export function setScore (context, payload) {
  context.commit('setScore', payload)
}

export function setInitialScore (context, payload) {
  context.commit('setInitialScore', payload)
}

export function logout (context) {
  context.commit('setLoadingLogout', true)
  return new Promise((resolve) => {
    setTimeout(() => {
      context.commit('setToken', null)
      context.commit('setRefreshToken', null)
      context.commit('setScore', 0)
      context.commit('setInitialScore', 0)
      context.commit('setLoadingLogout', false)
      resolve()
    }, 1000)
  })
}

export function login (context, payload) {
  // return fake.login(context)
  context.commit('setLoadingLogin', true)
  return new Promise((resolve, reject) => {
    const url = context.getters.urlLogin
    axios.post(url, payload)
      .then((result) => {
        if (!result.data.success) {
          return reject(result)
        }
        const user = result.data.data.user
        context.commit('setToken', result.data.data.access_token)
        context.commit('setRefreshToken', result.data.data.refresh_token)
        context.commit('setScore', user.score)
        context.commit('setInitialScore', user.score)
        context.commit('setIsAdmin', user.is_admin)
        context.commit('setLoadingLogin', false)
        resolve(result)
      })
      .catch((result) => {
        context.commit('setLoadingLogin', false)
        reject(result)
      })
  })
}

export function register (context, payload) {
  // return fake.register(context)
  context.commit('setLoadingRegister', true)
  return new Promise((resolve, reject) => {
    const url = context.getters.urlRegister
    axios.post(url, payload)
      .then((result) => {
        if (!result.data.success) {
          return reject(result)
        }
        const user = result.data.data.user
        context.commit('setToken', result.data.data.access_token)
        context.commit('setRefreshToken', result.data.data.refresh_token)
        context.commit('setScore', user.score)
        context.commit('setInitialScore', user.score)
        context.commit('setIsAdmin', user.is_admin)
        context.commit('setLoadingRegister', false)
        resolve()
      })
      .catch((result) => {
        context.commit('setLoadingRegister', false)
        reject(result)
      })
  })
}

export function profile (context, payload) {
  // return fake.profile(context)
  context.commit('setLoadingProfile', true)
  return new Promise((resolve, reject) => {
    let url = `${context.getters.url}/profile`
    if (payload.id) {
      url = `${context.getters.url}/${payload.id}`
    }
    axios.get(url)
      .then((result) => {
        if (!result.data.success) {
          return reject(result)
        }
        context.commit('setLoadingProfile', false)
        context.commit('setProfile', result.data.data)
      })
      .catch((result) => {
        context.commit('setLoadingProfile', false)
        reject(result)
      })
  })
}

export function getCoupons (context) {
  context.commit('setLoadingCoupons', true)
  return new Promise((resolve, reject) => {
    const coupons = Array.from(Array(20), (_, x) => {
      return {
        id: faker.random.uuid(),
        code: faker.random.word(),
        score: faker.random.number({
          min: 500,
          max: 2000
        })
      }
    })
    setTimeout(() => {
      context.commit('setLoadingCoupons', false)
      context.commit('setCoupons', coupons)
      resolve(profile)
    }, 1000)
  })
}

export function setLastPath (context, payload) {
  context.commit('setLastPath', payload)
}

export function getProducts (context) {
  context.commit('setLoadingProducts', true)
  return new Promise((resolve, reject) => {
    const products = Array.from(Array(20), (_, x) => {
      const urlImage = 'https://placeimg.com/640/480/animals'
      return {
        id: faker.random.uuid(),
        name: faker.commerce.productName(),
        description: faker.lorem.paragraphs(2),
        url_image: `${urlImage}?nc=${x}`,
        score: faker.random.number({
          min: 500,
          max: 2000
        })
      }
    })
    setTimeout(() => {
      context.commit('setLoadingProducts', false)
      context.commit('setProducts', products)
      resolve(profile)
    }, 1000)
  })
}
