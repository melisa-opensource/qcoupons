import faker from 'faker/locale/es_MX'

export function login (context) {
  context.commit('setLoadingLogin', true)
  return new Promise((resolve, reject) => {
    const score = faker.random.number({
      min: 1,
      max: 7
    })
    const result = {
      token: faker.random.uuid(),
      refresh_token: faker.random.uuid(),
      email: faker.internet.email(),
      isAdmin: faker.random.boolean(),
      score: score * 100
    }
    setTimeout(() => {
      context.commit('setLoadingLogin', false)
      context.commit('setToken', result.token)
      context.commit('setScore', result.score)
      context.commit('setInitialScore', result.score)
      context.commit('setIsAdmin', result.isAdmin)
      resolve(result)
    }, 1000)
  })
}

export function register (context) {
  context.commit('setLoadingRegister', true)
  return new Promise((resolve, reject) => {
    const result = {
      token: faker.random.uuid(),
      refresh_token: faker.random.uuid(),
      email: faker.internet.email(),
      is_admin: faker.random.boolean(),
      score: 0
    }
    setTimeout(() => {
      context.commit('setLoadingRegister', false)
      context.commit('setToken', result.token)
      context.commit('setScore', result.score)
      context.commit('setInitialScore', result.score)
      context.commit('setIsAdmin', result.is_admin)
      resolve(result)
    }, 1000)
  })
}

export function profile (context) {
  context.commit('setLoadingProfile', true)
  return new Promise((resolve, reject) => {
    const profile = {
      name: faker.name.findName(),
      score: faker.random.number({
        min: 1000,
        max: 3000
      })
    }
    setTimeout(() => {
      context.commit('setLoadingProfile', false)
      context.commit('setProfile', profile)
      resolve(profile)
    }, 1000)
  })
}
