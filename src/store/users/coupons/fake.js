import faker from 'faker/locale/es_MX'

export function add (context) {
  context.commit('setLoadingAdd', true)
  return new Promise((resolve, reject) => {
    const score = faker.random.number({
      min: 1,
      max: 7
    })
    const result = {
      success: faker.random.boolean(),
      data: {
        score: score * 100
      }
    }
    const currentScore = context.rootGetters['users/score']
    result.data.score += currentScore
    if (!result.success) {
      result.errors = [
        {
          code: faker.random.word()
        }
      ]
    }
    setTimeout(() => {
      context.commit('setLoadingAdd', false)
      if (result.success) {
        return resolve(result)
      }
      reject(result)
    }, 1000)
  })
}
