// import faker from 'faker/locale/es_MX'
// import * as fake from './fake'
import axios from 'axios'
import * as http from 'src/helpers/StoreActions'

export function paging (context, payload) {
  context.commit('setLoading', false)
  return new Promise((resolve, reject) => {
    return http.paging(context, payload)
      .then((result) => {
        if (!result.data.success) {
          return reject(result)
        }
        context.commit('setLoading', false)
        context.commit('setRecords', result.data.data)
        resolve(result.data.data)
      })
      .catch((result) => {
        reject(result)
      })
  })
}

export function add (context, payload) {
  // return fake.add(context)
  context.commit('setLoadingAdd', true)
  return new Promise((resolve, reject) => {
    const url = context.getters.url
    axios.post(url, payload)
      .then((result) => {
        if (!result.data.success) {
          return reject(result)
        }
        context.commit('setLoadingAdd', false)
        resolve(result.data.data)
      })
      .catch((result) => {
        context.commit('setLoadingAdd', false)
        reject(result)
      })
  })
}
