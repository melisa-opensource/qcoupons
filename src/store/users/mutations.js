import axios from 'axios'
import { LocalStorage } from 'quasar'

export function setLoadingProfile (state, payload) {
  state.loadingProfile = payload
}
export function setLoadingCoupons (state, payload) {
  state.loadingCoupons = payload
}
export function setLoadingProducts (state, payload) {
  state.loadingProducts = payload
}
export function setProfile (state, payload) {
  state.profile = payload
}
export function setCoupons (state, payload) {
  state.coupons = payload
}
export function setProducts (state, payload) {
  state.products = payload
}
export function setLoadingLogin (state, payload) {
  state.loadingLogin = payload
}
export function setLoadingLogout (state, payload) {
  state.loadingLogout = payload
}
export function setLoadingRegister (state, payload) {
  state.loadingRegister = payload
}
export function setRefreshToken (state, payload) {
  if (!payload) {
    state.refreshToken = null
    LocalStorage.remove('security.refresh_token')
    return
  }
  state.refreshToken = payload
  LocalStorage.set('security.refresh_token', state.refreshToken)
}
export function setToken (state, payload) {
  if (!payload) {
    state.token = null
    LocalStorage.remove('security.token')
    LocalStorage.remove('security.refresh_token')
    LocalStorage.remove('security.is_admin')
    return
  }
  state.token = payload
  axios.defaults.headers.common['Authorization'] = `Bearer ${payload}`
  LocalStorage.set('security.token', state.token)
}
export function setScore (state, payload) {
  LocalStorage.set('security.score', payload)
  state.score = payload
}
export function setInitialScore (state, payload) {
  state.initialScore = payload
}
export function setLastPath (state, payload) {
  state.lastPath = payload
}
export function setIsAdmin (state, payload) {
  state.isAdmin = payload
  LocalStorage.set('security.is_admin', payload)
}
