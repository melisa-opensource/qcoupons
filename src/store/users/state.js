import { LocalStorage } from 'quasar'
import axios from 'axios'

const token = LocalStorage.getItem('security.token') || null
const refreshToken = LocalStorage.getItem('security.refresh_token') || null
const isAdmin = LocalStorage.getItem('security.is_admin') || null
const score = LocalStorage.getItem('security.score') || 0
if (token) {
  axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
}

export default function () {
  return {
    urlLogin: '/api/v1/login',
    urlRegister: '/api/v1/register',
    url: '/api/v1/users',
    score: score,
    initialScore: score,
    token: token,
    refreshToken: refreshToken,
    profile: null,
    loadingProfile: true,
    loadingCoupons: true,
    loadingProducts: true,
    loadingLogin: false,
    loadingLogout: false,
    loadingRegister: false,
    isAdmin: isAdmin,
    coupons: [],
    products: [],
    lastPath: null
  }
}
