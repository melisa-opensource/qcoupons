import Vue from 'vue'
import axios from 'axios'

axios.defaults.baseURL = process.env.API_URL
axios.defaults.headers.common['client_id'] = process.env.CLIENT_ID
axios.defaults.headers.common['Content-Type'] = 'application/json'
// fix necesary, or not include header Content-Type in request type GET
axios.defaults.data = {}

Vue.prototype.$axios = axios
