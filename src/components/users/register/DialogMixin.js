import ShowMixin from 'src/helpers/ShowMixin'

export default {
  mixins: [
    ShowMixin
  ],
  computed: {
    loading () {
      return this.$store.getters['users/loadingRegister']
    }
  },
  methods: {
    onClickBtnLogin () {
      this.$emit('show-login')
      this.isShow = false
    },
    onClickBtnRegister () {
      if (!this.$refs.form.isValidForm()) {
        return
      }
      const values = this.$refs.form.getFormValues()
      this.$store.dispatch('users/register', values)
        .then(this.onSuccessRegister)
    },
    onSuccessRegister () {
      this.isShow = false
    }
  }
}
