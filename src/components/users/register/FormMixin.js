import { required, minLength, sameAs, email } from 'vuelidate/lib/validators'
import FormMixin from 'src/helpers/FormMixin'
import PasswordComplexity from 'src/helpers/PasswordComplexity'
import GeneratePasswordMixin from 'src/helpers/GeneratePasswordMixin'

export default {
  mixins: [
    FormMixin,
    GeneratePasswordMixin
  ],
  data () {
    return {
      isPwd: true,
      isPwdR: true,
      form: {
        name: null,
        email: null,
        password: null,
        passwordRepeat: null
      }
    }
  },
  validations: {
    form: {
      name: {
        required
      },
      email: {
        email,
        required
      },
      password: {
        required,
        minLength: minLength(6),
        PasswordComplexity
      },
      passwordRepeat: {
        required,
        minLength: minLength(6),
        sameAsPassword: sameAs('password'),
        PasswordComplexity
      }
    }
  },
  computed: {
    getNameError () {
      if (!this.$v.form.name.$error) {
        return ''
      }
      return 'El nombre es requerido'
    },
    getEmailError () {
      if (this.$v.form.email.$error) {
        return 'Ingresa un correo valido'
      }
      return ''
    },
    getPasswordError () {
      if (!this.$v.form.password.$error) {
        return ''
      }
      if (!this.$v.form.password.minLength) {
        return `Ingresa por lo menos ${this.$v.form.password.$params.minLength.min} caracteres`
      }
      if (!this.$v.form.password.passwordComplexity) {
        return 'Ingrese por lo menos una letra Mayuscula y un número'
      }
      return ''
    },
    getPasswordRepeatError () {
      if (!this.$v.form.passwordRepeat.minLength) {
        return `Ingresa por lo menos ${this.$v.form.passwordRepeat.$params.minLength.min} caracteres`
      }
      if (!this.$v.form.passwordRepeat.sameAsPassword) {
        return 'Las contraseñas no son iguales'
      }
      if (!this.$v.form.passwordRepeat.passwordComplexity) {
        return 'Ingrese por lo menos una letra Mayuscula y un número'
      }
      return ''
    }
  },
  methods: {
    onClickGeneratePassword () {
      this.form.password = this.form.passwordRepeat = this.generatePassword()
      this.isPwd = false
      setTimeout(() => {
        this.isPwd = true
      }, 1000)
    }
  }
}
