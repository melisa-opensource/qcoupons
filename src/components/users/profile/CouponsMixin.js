export default {
  data () {
    return {
      columns: [
        {
          field: 'code',
          label: 'Cupon',
          align: 'left'
        },
        {
          field: 'score',
          label: 'Puntos',
          align: 'left'
        }
      ]
    }
  },
  mounted () {
    this.$store.dispatch('users/getCoupons')
  },
  computed: {
    loading () {
      return this.$store.getters['users/loadingCoupons']
    },
    coupons () {
      return this.$store.getters['users/coupons']
    }
  }
}
