export default {
  mounted () {
    this.$store.dispatch('users/profile', this.$route.params)
  },
  computed: {
    loading () {
      return this.$store.getters['users/loadingProfile']
    },
    profile () {
      return this.$store.getters['users/profile']
    }
  }
}
