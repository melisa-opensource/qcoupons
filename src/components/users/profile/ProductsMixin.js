export default {
  data () {
    return {
      columns: [
        {
          field: 'score',
          label: 'Puntos',
          align: 'left'
        },
        {
          field: 'name',
          label: 'Nombre',
          align: 'left'
        }
      ]
    }
  },
  mounted () {
    this.$store.dispatch('users/getProducts')
  },
  computed: {
    loading () {
      return this.$store.getters['users/loadingProducts']
    },
    products () {
      return this.$store.getters['users/products']
    }
  }
}
