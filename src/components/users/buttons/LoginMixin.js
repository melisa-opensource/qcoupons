import ShowDialogLoginMixin from 'src/helpers/ShowDialogLoginMixin'
import ShowDialogRegisterMixin from 'src/helpers/ShowDialogRegisterMixin'

export default {
  mixins: [
    ShowDialogLoginMixin,
    ShowDialogRegisterMixin
  ],
  computed: {
    getLabel () {
      if (this.$q.platform.is.mobile) {
        return ''
      }
      return 'Iniciar sesión'
    },
    getIcon () {
      if (this.$q.platform.is.mobile) {
        return 'account_circle'
      }
      return 'fas fa-sign-in-alt'
    }
  },
  methods: {
    onClickBtnLogin () {
      this.showDialogLogin = true
    }
  }
}
