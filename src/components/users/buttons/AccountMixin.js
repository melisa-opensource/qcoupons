export default {
  computed: {
    getLabel () {
      if (this.$q.platform.is.mobile) {
        return ''
      }
      return 'Mi cuenta'
    },
    getIcon () {
      if (this.$q.platform.is.mobile) {
        return 'account_circle'
      }
      return ''
    }
  },
  methods: {
    onClickLogout () {
      this.$q.loading.show({
        message: 'Cerrando sesión'
      })
      this.$store.dispatch('users/logout')
        .then(this.onSuccessLogout)
        .catch(this.onErrorLogout)
    },
    onErrorLogout () {
      this.$q.loading.hide()
    },
    onSuccessLogout () {
      this.$q.loading.hide()
      this.$router.push({
        name: 'home'
      })
    }
  }
}
