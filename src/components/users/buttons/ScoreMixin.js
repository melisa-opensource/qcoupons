export default {
  computed: {
    getLabelScore () {
      if (this.$q.platform.is.mobile) {
        return this.score
      }
      return `${this.score} puntos`
    },
    getClass () {
      if (this.$q.platform.is.mobile) {
        return 'text-positive text-bold'
      }
      return ''
    }
  },
  methods: {
    onClickBtnScore () {

    }
  }
}
