import ShowMixin from 'src/helpers/ShowMixin'

export default {
  mixins: [
    ShowMixin
  ],
  computed: {
    loading () {
      return this.$store.getters['users/loadingLogin']
    }
  },
  methods: {
    onClickBtnRegister () {
      this.$emit('show-register')
      this.isShow = false
    },
    onClickBtnLogin () {
      if (!this.$refs.form.isValidForm()) {
        return
      }
      const values = this.$refs.form.getFormValues()
      this.$store.dispatch('users/login', values)
        .then(this.onSuccessLogin)
    },
    onSuccessLogin () {
      this.$refs.form.reset()
      this.isShow = false
    }
  }
}
