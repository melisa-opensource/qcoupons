import { required, email } from 'vuelidate/lib/validators'
import FormMixin from 'src/helpers/FormMixin'

export default {
  mixins: [
    FormMixin
  ],
  data () {
    return {
      showPassword: true,
      form: {
        email: '',
        password: ''
      }
    }
  },
  validations: {
    form: {
      email: {
        required,
        email
      },
      password: {
        required
      }
    }
  },
  computed: {
    getEmailError () {
      if (this.$v.form.email.$error) {
        return 'Ingresa tú correo'
      }
      return ''
    },
    getPasswordError () {
      if (this.$v.form.password.$error) {
        return 'Ingresa la contraseña'
      }
      return ''
    }
  },
  methods: {
    reset () {
      this.form = {
        email: '',
        password: ''
      }
    }
  }
}
