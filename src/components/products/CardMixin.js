export default {
  methods: {
    onClickProduct (item) {
      this.$router.push({
        name: 'products.report',
        params: item
      })
    }
  }
}
