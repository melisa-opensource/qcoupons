import { required } from 'vuelidate/lib/validators'
import FormMixin from 'src/helpers/FormMixin'

export default {
  mixins: [
    FormMixin
  ],
  data () {
    return {
      examples: [
        'https://placeimg.com/640/480/animals',
        'https://placeimg.com/640/480/arch',
        'https://placeimg.com/640/480/nature',
        'https://placeimg.com/640/480/people',
        'https://placeimg.com/640/480/tech',
        'https://placeimg.com/640/480/any'
      ],
      form: {
        name: '',
        score: '',
        url_image: 'https://placeimg.com/640/480/animals',
        description: ''
      }
    }
  },
  validations: {
    form: {
      name: {
        required
      },
      score: {
        required
      },
      url_image: {
        required
      },
      description: {}
    }
  },
  computed: {
    getNameError () {
      if (this.$v.form.name.$error) {
        return 'Ingresa el nombre'
      }
      return ''
    },
    getScoreError () {
      if (this.$v.form.score.$error) {
        return 'Ingresa la puntuación'
      }
      return ''
    },
    getUrlImageError () {
      if (this.$v.form.url_image.$error) {
        return 'Ingresa la URL a la imagen'
      }
      return ''
    }
  },
  methods: {
    onClickBtnExample (url) {
      this.form.url_image = url
      this.$refs.txtName.focus()
    }
  }
}
