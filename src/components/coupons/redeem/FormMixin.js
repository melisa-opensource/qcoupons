import { required, minLength } from 'vuelidate/lib/validators'
import FormMixin from 'src/helpers/FormMixin'

export default {
  mixins: [
    FormMixin
  ],
  data () {
    return {
      form: {
        code: ''
      }
    }
  },
  validations: {
    form: {
      code: {
        required,
        minLength: minLength(6)
      }
    }
  },
  computed: {
    getCodeError () {
      if (this.$v.form.code.$error) {
        return 'Ingresa el cupón, son 6 caracteres'
      }
      return ''
    }
  }
}
