import ShowMixin from 'src/helpers/ShowMixin'
import ErrorsMixin from 'src/helpers/ErrorsMixin'

export default {
  mixins: [
    ShowMixin,
    ErrorsMixin
  ],
  computed: {
    loading () {
      return this.$store.getters['usersCoupons/loadingAdd']
    }
  },
  methods: {
    onClickBtnAdd () {
      if (!this.$refs.form.isValidForm()) {
        return
      }
      const values = this.$refs.form.getFormValues()
      this.$store.dispatch('usersCoupons/add', values)
        .then(this.onSuccessAdd)
        .catch(this.onCatchAdd)
    },
    onCatchAdd (result) {
      this.notifyErrors(result)
    },
    onSuccessAdd (result) {
      this.$store.dispatch('users/setInitialScore', result.score)
      this.$store.dispatch('users/setScore', result.score)
      this.isShow = false
      this.$q.notify({
        color: 'positive',
        message: `Ahora tienes ${result.score} puntos`
      })
    }
  }
}
