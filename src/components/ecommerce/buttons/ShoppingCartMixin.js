export default {
  computed: {
    totalRecords () {
      return this.$store.getters['shoppingCart/totalRecords']
    }
  }
}
