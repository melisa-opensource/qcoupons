import ShowDialogLoginMixin from 'src/helpers/ShowDialogLoginMixin'
import ShowDialogRegisterMixin from 'src/helpers/ShowDialogRegisterMixin'

export default {
  mixins: [
    ShowDialogLoginMixin,
    ShowDialogRegisterMixin
  ],
  data () {
    return {
      showDialogCoupons: false
    }
  },
  computed: {
    enoughCoupons () {
      return this.score >= this.value.score
    },
    getLabel () {
      return `Te faltan ${this.value.score - this.score}`
    }
  },
  methods: {
    onClickBtnAdd () {
      console.log('onClickBtnAdd', this.value)
      this.showDialogLogin = true
    },
    onClickBtnShowDialogCoupons () {
      this.showDialogCoupons = true
    },
    onClickBtnDialogAddCart () {
      this.$store.dispatch('shoppingCart/add', this.value)
        .then(this.onSuccessAdd)
        .catch(this.onCatchAdd)
    },
    onCatchAdd () {},
    onSuccessAdd (result) {
      this.$store.dispatch('users/setScore', result.data.score)
    },
    getComponentShowDialogCoupons () {
      return import('src/components/coupons/redeem/Dialog')
    }
  }
}
