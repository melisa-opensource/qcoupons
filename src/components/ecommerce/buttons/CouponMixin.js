import ShowDialogCouponMixin from 'src/helpers/ShowDialogCouponMixin'

export default {
  mixins: [
    ShowDialogCouponMixin
  ],
  methods: {
    onClickBtn () {
      this.showDialogCoupon = true
    }
  }
}
