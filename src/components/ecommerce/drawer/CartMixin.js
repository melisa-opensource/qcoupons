export default {
  computed: {
    show: {
      get () {
        return this.$store.getters['shoppingCart/show']
      },
      set (val) {
        this.$store.dispatch('shoppingCart/setShow', val)
      }
    },
    records: {
      get () {
        return this.$store.getters['shoppingCart/records']
      },
      set (val) {
        this.$store.dispatch('shoppingCart/setRecords', val)
      }
    }
  },
  methods: {
    onClickBtnRemove (id) {
      this.$store.dispatch('shoppingCart/remove', id)
        .then(this.onSuccessRemove)
    },
    onSuccessRemove (result) {
      this.$store.dispatch('users/setScore', result.data.score)
    },
    onClickBtnBuy () {
      this.$q.loading.show({
        message: 'Registrando compra'
      })
      this.$store.dispatch('shoppingCart/buy')
        .then(this.onSuccessBuy)
        .catch(this.onCatchBuy)
    },
    onSuccessBuy () {
      this.$q.loading.hide()
      this.$q.notify({
        color: 'positive',
        message: `Compra registrada correctamente`
      })
    },
    onCatchBuy () {
      this.$q.loading.hide()
      this.$q.notify({
        color: 'negative',
        message: `Huvo un problema, intentalo nuevamente`
      })
    }
  }
}
