import { ifAuthenticated, ifAdmin } from 'src/helpers/RouteSecurity'

const routes = [
  {
    path: '/',
    component: () => import('layouts/Default.vue'),
    children: [
      {
        path: '',
        name: 'home',
        component: () => import('pages/Index.vue')
      },
      {
        path: 'profile',
        name: 'profile',
        beforeEnter: ifAuthenticated,
        component: () => import('pages/Profile.vue')
      },
      {
        path: 'products',
        redirect: {
          name: 'home'
        }
      },
      {
        path: 'products/add',
        name: 'products.add',
        component: () => import('pages/products/Add.vue')
      },
      {
        path: 'products/:id',
        name: 'products.report',
        component: () => import('pages/products/Report.vue')
      },
      {
        path: 'ranking',
        name: 'ranking',
        component: () => import('pages/Ranking.vue')
      },
      {
        path: 'coupons',
        name: 'coupons',
        beforeEnter: ifAdmin,
        component: () => import('pages/Coupons.vue')
      }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
