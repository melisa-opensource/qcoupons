export default {
  data () {
    return {
      isShow: this.show
    }
  },
  watch: {
    isShow (value) {
      this.$emit('update:show', value)
    },
    show (value) {
      this.isShow = value
    }
  }
}
