import axios from 'axios'

export function buildParamsPaging (context, payload) {
  if (!payload) {
    payload = {
      pagination: context.getters.pagination
    }
  }
  if (!payload.pagination) {
    payload.pagination = context.getters.pagination
  }
  return {
    page: payload.page || payload.pagination.page,
    limit: payload.pagination.rowsPerPage,
    start: 0,
    filter: null
  }
}

export function paging (context, payload) {
  const params = buildParamsPaging(context, payload)
  const url = context.getters.url
  return axios.get(url, {
    params: params
  })
}
