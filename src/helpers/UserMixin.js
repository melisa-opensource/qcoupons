export default {
  computed: {
    isAuthenticated () {
      return this.$store.getters['users/isAuthenticated']
    },
    score () {
      return this.$store.getters['users/score']
    }
  }
}
