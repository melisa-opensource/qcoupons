export default {
  methods: {
    parseErrors (result) {
      if (!result) {
        return [ 'Error desconocido' ]
      }
      if (typeof result.errors !== 'object' && !result.errors) {
        return
      }
      return result.errors.map((error) => {
        return this.$t(error.code || 'Error desconocido')
      })
    },
    notifyErrors (result) {
      const messages = this.parseErrors(result)
      this.$q.notify({
        color: 'negative',
        message: messages.join(' ')
      })
    }
  }
}
