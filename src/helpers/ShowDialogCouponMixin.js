export default {
  data () {
    return {
      showDialogCoupon: false
    }
  },
  methods: {
    getComponentDialogCoupon () {
      return import('src/components/coupons/redeem/Dialog')
    }
  }
}
