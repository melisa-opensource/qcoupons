export const findById = (array, id) => {
  return array.findIndex(item => item.id === id)
}

export const findByField = (array, value, field) => {
  return array.findIndex(item => item[field] === value)
}

export const isUniqueId = (array, id) => {
  return findById(array, id) === -1
}

export const removeById = (array, id) => {
  array.splice(findById(array, id), 1)
}
