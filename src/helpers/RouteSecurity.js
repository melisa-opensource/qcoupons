import { Platform } from 'quasar'
import store from 'src/store/index'

export const isMobile = Platform.is.mobile
export const mobile = isMobile ? '.mobile' : ''

export const ifNotAuthenticated = (to, from, next) => {
  if (!store.getters['isAuthenticated']) {
    return next()
  }
  next('/')
}

export const ifAuthenticated = (to, from, next) => {
  if (store.getters['users/isAuthenticated']) {
    return next()
  }
  store.dispatch('users/setLastPath', to.path)
  next('/')
}

export const ifAdmin = (to, from, next) => {
  if (!ifAuthenticated(to, from, next)) {
    return
  }
  if (!store.getters('users/isAdmin')) {
    return next('/')
  }
  next()
}
