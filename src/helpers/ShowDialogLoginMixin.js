export default {
  data () {
    return {
      showDialogLogin: false
    }
  },
  methods: {
    getComponentDialogLogin () {
      return import('src/components/users/login/Dialog')
    }
  }
}
