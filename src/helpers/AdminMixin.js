export default {
  computed: {
    isAdmin () {
      return this.$store.getters['users/isAdmin']
    }
  }
}
