export default password => {
  // minimum of 1 uppercase letter
  if (/[A-Z]/.test(password) === false) {
    return false
  }
  // minimum of 1 number
  if (/\d/.test(password) === false) {
    return false
  }
  return true
}
