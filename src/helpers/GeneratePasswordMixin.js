export default {
  methods: {
    generatePassword (numLowerCase, numUpperCase, numDigits, numSpecial) {
      numLowerCase = numLowerCase || 3
      numUpperCase = numUpperCase || 2
      numDigits = numDigits || 2
      numSpecial = numSpecial || 1
      let lcLetters = 'abcdefghijklmnopqrstuvwxyz'
      let ucLetters = lcLetters.toUpperCase()
      let numbers = '0123456789'
      let special = '!@#$%&*()-_=+'
      let shuffle = function (o) {
        for (let j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x) {}
        return o
      }
      let getRand = function (values) {
        return values.charAt(Math.floor(Math.random() * values.length))
      }
      let pass = []
      for (let i = 0; i < numLowerCase; ++i) { pass.push(getRand(lcLetters)) }
      for (let i = 0; i < numUpperCase; ++i) { pass.push(getRand(ucLetters)) }
      for (let i = 0; i < numDigits; ++i) { pass.push(getRand(numbers)) }
      for (let i = 0; i < numSpecial; ++i) { pass.push(getRand(special)) }
      return shuffle(pass).join('')
    }
  }
}
