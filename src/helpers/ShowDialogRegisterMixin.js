export default {
  data () {
    return {
      showDialogRegister: false
    }
  },
  methods: {
    getComponentDialogRegister () {
      return import('src/components/users/register/Dialog')
    }
  }
}
