export default {
  methods: {
    getFormValues () {
      return this.form
    },
    isValidForm () {
      this.$v.form.$touch()
      if (this.$v.form.$error) {
        this.$q.notify({
          color: 'negative',
          message: 'Formulario invalido'
        })
        return false
      }
      return true
    },
    setFormValues (values) {
      this.form = values
    }
  }
}
